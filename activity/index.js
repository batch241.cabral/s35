const { urlencoded } = require("express");
const express = require("express");
const app = express();
const mongoose = require("mongoose");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect(
    "mongodb+srv://admin123:admin123@wdc028-course-booking.rtpbcvq.mongodb.net/s35Activity?retryWrites=true&w=majority",
    { useNewUrlParser: true, useUnifiedTopology: true }
);

const userSchema = new mongoose.Schema({
    username: String,
    password: String,
    status: {
        type: String,
        default: "pending",
    },
});

const User = mongoose.model("User", userSchema);

let db = mongoose.connection;
db.on("err", console.error.bind(console, "Error on connecting to database"));
db.once("open", () => console.log("Connected"));

app.post("/signup", (req, res) => {
    User.findOne({ username: req.body.username }, (err, result) => {
        if (result != null && result.username == req.body.username) {
            res.send("Duplicate found!");
        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password,
            });
            newUser.save((saveErr, saveUser) => {
                if (saveErr) {
                    return console.log(saveErr);
                } else {
                    return res.status(201).send("New user registered");
                }
            });
        }
    });
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening to port ${port}...`));
