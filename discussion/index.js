const express = require("express");
const mongoose = require("mongoose");

const app = express();

const port = process.env.PORT || 3000;

mongoose.connect(
    "mongodb+srv://admin123:admin123@wdc028-course-booking.rtpbcvq.mongodb.net/s35?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const nameSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    status: {
        type: String,
        default: "pending",
    },
});

const NameSc = mongoose.model("NameSc", nameSchema);

app.post("/postNames", (req, res) => {
    NameSc.findOne({ firstName: req.body.firstName }, (err, result) => {
        if (result != null && result.firstName == req.body.firstName) {
            return res.status(401).send("Duplicate found!");
        } else if (
            !req.body ||
            typeof req.body !== "object" ||
            Object.keys(req.body).length === 0
        ) {
            return res.send("Can't return an empty file");
        } else if (
            !req.body.hasOwnProperty("firstName") ||
            !req.body.firstName ||
            !req.body.hasOwnProperty("lastName") ||
            !req.body.lastName
        ) {
            return res.send("Property is missing or empty");
        } else {
            let newNameSc = new NameSc({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
            });
            newNameSc.save((saveErr, saveNewNameSc) => {
                if (saveErr) {
                    return console.log(saveErr);
                } else {
                    return res.status(201).send("Data saved successfully");
                }
            });
        }
    });
});

app.get("/get", (req, res) => {
    NameSc.find({}, (err, result) => {
        if (err) {
            return console.log(err);
        } else {
            return res.status(200).json({
                data: result,
            });
        }
    });
});

app.listen(port, () => console.log(`Server is running on port ${port}...`));
